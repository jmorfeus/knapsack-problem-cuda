#pragma once
#include "Includes.h"
#include "Item.h"
class KnapsackSolver
{
public:
	KnapsackSolver();
	KnapsackSolver(int M, int N, int * weights, int * values);
	virtual bool Solve() = 0;
	void SetRecursive(bool in);
	void PrintParams();
	void PrintResult();
protected:
	int m_M;
	int m_N;
	int m_current_best_price;
	Item * m_items;
	bool m_recursive = false;
	void Log(std::string in);
	bool CheckIfBest(int currWeight, int currValue);
};
