#include "KnapsackSolver.h"
#include "BruteKnapsackSolver.h"
#include "BoundKnapsackSolver.h"
#include "DynamicKnapsackSolver.h"

using namespace std;

// nacteni dat ze souboru
bool ReadFileData(string filename, int * M, int * N, int * &weights, int * &values)
{
	string line;
	fstream f;
	int num;
	f.open(filename, ios_base::in);
	if (f.fail())
	{
		cout << "Soubor neexistuje.";
		return false;
	}
	getline(f, line);
	*M = std::stoi(line);
	getline(f, line);
	*N = std::stoi(line);
	weights = new int[*N + 1];
	values = new int[*N + 1];
	weights[0] = 0;
	values[0] = 0;
	
	getline(f, line);
	istringstream is1(line);
	for (int i = 1; i <= *N; i++)
	{
		is1 >> num;
		weights[i] = num;
	}

	getline(f, line);
	istringstream is2(line);
	for (int i = 1; i <= *N; i++)
	{
		is2 >> num;
		values[i] = num;
	}
	return true;
}

int main(int argc, char* argv[])
{
	int M, N;
	int * weights;
	int * values;
	
	if (argc <= 1)
	{
		cout << "File not specified." << endl;
		return 0;
	}
	if (!ReadFileData(argv[1], &M, &N, weights, values))
	{
		#ifdef _WIN32
		cin.get();
		#endif
		return 0;
	}
	KnapsackSolver* solver;
	if (argc > 2)
	{
		if ((string)argv[2] == "dynamic")  solver = new DynamicKnapsackSolver(M, N, weights, values);
		else if ((string)argv[2] == "bound")
		{
			solver = new BoundKnapsackSolver(M, N, weights, values);
			if (argc > 3 && (string)argv[3] == "r") solver->SetRecursive(true);
		}
	}
	else
	{
		// vyber metody vypoctu
		//KnapsackSolver* solver = new BoundKnapsackSolver(M, N, weights, values);
		//KnapsackSolver* solver = new BruteKnapsackSolver(M, N, weights, values);
		//solver->SetRecursive(false);
		solver = new DynamicKnapsackSolver(M, N, weights, values);
	}


	solver->PrintParams();
	cin.get();
	if (solver->Solve())
	{
		solver->PrintResult();
	}

	delete []weights;
	delete []values;
	delete solver;

	#ifdef _WIN32
	cin.get();
	#endif
	return 0;
}

