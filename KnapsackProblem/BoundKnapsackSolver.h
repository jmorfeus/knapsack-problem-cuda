#pragma once
#include "TreeKnapsackSolver.h"
class BoundKnapsackSolver : public TreeKnapsackSolver
{
public:
	BoundKnapsackSolver() :TreeKnapsackSolver(){ m_current_upperBound = 0; };
	BoundKnapsackSolver(int M, int N, int * weights, int * values) : TreeKnapsackSolver(M, N, weights, values)
	{
		cout << "Creating BoundKnapsackSolver" << endl;
		SortItems(); 
		m_current_upperBound = 0;
	};
protected:
	int GetMaxBranchPrice(int currItem);
	bool IsPerspective(Item node); 
	int CalculateUpperBound(Item node);
	int m_current_upperBound;
	void SortItems();
	static int CompareItems(const void * a, const void * b);
};
