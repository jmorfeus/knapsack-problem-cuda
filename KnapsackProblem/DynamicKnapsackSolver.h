#pragma once
#include "KnapsackSolver.h"
class DynamicKnapsackSolver : public KnapsackSolver
{
public:
	bool Solve();
	DynamicKnapsackSolver() :KnapsackSolver(){};
	DynamicKnapsackSolver(int M, int N, int * weights, int * values) : KnapsackSolver(M, N, weights, values){
		cout << "Creating DynamicKnapsackSolver" << endl;
	};
protected:

};
