#include "KnapsackSolver.h"

using namespace std;

KnapsackSolver::KnapsackSolver(int M, int N, int * weights, int * values)
{
	this->m_current_best_price = 0;
	this->m_M = M;
	this->m_N = N;
	Item* items = new Item[m_N+1];

	for (int i = 0; i <= m_N; i++)
	{
		items[i].weight = weights[i];
		items[i].value = values[i];
	}

	for (int i = 1; i <= m_N; i++)
	{
		items[i].level = i;
	}

	m_items = items;
}


void KnapsackSolver::PrintParams()
{
	cout << "Knapsack size: " << this->m_M << endl;
	cout << "Item count: " << this->m_N << endl;
	cout << "Items: ";
	Tools::printArray(this->m_items, this->m_N);
}

void KnapsackSolver::PrintResult()
{
	cout << "Best price: " << this->m_current_best_price << endl;
}


bool KnapsackSolver::CheckIfBest(int currWeight, int currValue)
{
	return (currWeight <= this->m_M && currValue > this->m_current_best_price) ? true : false;
}

void KnapsackSolver::SetRecursive(bool in)
{
	cout << "Setting recursive to " << in << endl;
	m_recursive = in;
}
KnapsackSolver::KnapsackSolver()
{
}
